// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract EventContract is Ownable {

  uint256 private id;
  uint256 private amountToRaise;
  string private uri;

  address private fundReceiver;
  address private feeReceiver;

  uint256 public totalContribution;
  
  uint256 public fee = 250; // 2.5% of the transaction
  uint256 private feesCollected; // Total fees collected by the contract
  uint256 private feesWithdrawn; // Fees withdrawn

  uint256 public fundWithdrawn; // Funds withdrawn by the event creator

  uint8 public changeLimit = 3; // Can change amountToRaise for 3 times
  uint8 public timesChanged; // Checks the number of time the amountToRaise was change

  // user address => amount contributed
  mapping(address => uint256) public _contributors;
  
  event ChangeRaisableAmount(uint256 amount);
  event Contributed(address by, uint256 amount, string message);
  event FeeChanged(address new_fee);
  event FundsWithdrawn(address by, uint256 amount);
  event FeeWithdrawn(uint256 amount);

  modifier isReceiver() {
    require(feeReceiver == msg.sender, "Must be fee receiver to use the function!");
    _;
  }

  constructor(uint256 _id, string _uri, uint256 _amountToRaise, address _fundReceiver, address _feeReceiver) public {
    id            = _id;
    uri           = _uri;
    amountToRaise = _amountToRaise;
    fundReceiver  = _fundReceiver;
    feeReceiver   = _feeReceiver;
  }

  function getBasicDetails() public view returns (
    uint256 _id, string _uri, uint256 _amountToRaise, uint256 _totalContribution 
  ) {
    _id = id; 
    _uri = uri;
    _amountToRaise = amountToRaise;
    _totalContribution = totalContribution;
  }

  function getFundReceiver() public view returns (address) {
    return fundReceiver;
  }
  function getFeeReceiver() public view returns (address) {
    return feeReceiver;
  }

  function getWithdrawableFunds() public view returns (uint256) {
    uint256 remainingFees = feesCollected - feesWithdrawn;
    uint256 fund = address(this).balance - remainingFees;
    return fund;
  }

  function getWithdrawableFees() public view returns (uint256) {
    uint256 remainingFees = feesCollected - feesWithdrawn;
    return remainingFees;
  }

  function getAddressContribution(address _user) public view returns (uint256) {
    return _contributors[_user];
  }

  function setUri(string _uri) external {
    uri = _uri;
  }

  function changeTxnFee(uint256 _fee) external onlyOwner {
    fee = _fee;
    emit FeeChanged(_fee);
  }

  function setNewLimit(uint8 _times) public isReceiver {
    changeLimit = _times;
  }

  function changeAmountToRaise(uint256 _amt) public {
    require(msg.sender == fundReceiver, "Only fund receiver/creator can change the amount!");
    require(_amt > amountToRaise, "Only able to increase the amount!");
    require(timesChanged <= changeLimit, "Can only change the amount for limited times!");
    
    timesChanged++;
    amountToRaise = _amt;

    emit ChangeRaisableAmount(_amt);
  }

  function makeContribution(string _msg) public payable {
    require(msg.value > 0, "EventContract: Contribution must be greater than 0!");

    _contributors[msg.sender] += msg.value;
    totalContribution += msg.value;
    feesCollected += ((msg.value * fee) / 10000);
    emit Contributed(msg.sender, msg.value, _msg);
  }

  function withdrawEventFund() public {
    require(msg.sender == fundReceiver, "Only fund receiver of the contract can initiate withdraw!");
    uint256 remainingFees = feesCollected - feesWithdrawn;

    require(address(this).balance > remainingFees, "No fund remaining to withdraw!");
    
    uint256 amtToSend = address(this).balance - remainingFees;
    fundWithdrawn += amtToSend;

    (bool sent, ) = payable(fundReceiver).call{value: amtToSend}("");
    require(sent, "Failed withdrawing event fund!");
    
    emit FundsWithdrawn(msg.sender, amtToSend);
  }

  function withdrawFees() public {
    uint256 remainingFees = feesCollected - feesWithdrawn;
    require(address(this).balance >= remainingFees, "Balance not enough!");

    feesWithdrawn += remainingFees;

    (bool sent, ) = payable(getFeeReceiver()).call{value: remainingFees}("");
    require(sent, "Could not withdraw fees!");

    emit FeeWithdrawn(remainingFees);
  }

  receive() external payable {
    makeContribution("");
  }
  fallback() external payable {
    makeContribution("");
  }

}