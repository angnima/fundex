// SPDX-License-Identifier: MIT
pragma solidity 0.8.10;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

import "./EventContract.sol";

/**
  @title InitiatorContract for which creates donation events
  @author Angnima Sherpa
*/
contract InitiatorContract is Ownable {
  
  uint256 public counter;
  AggregatorV3Interface internal priceFeed;

  uint8 private fees = 5; // 5% fee. Calculates using chainlink pricefeed
  address payable private feeBeneficiary; // collects fee from events

  /**
    @dev Intantiates priceFeed contract and fee receiver
    @param _feedAddress Pass address of ETH or Native Token of chain
    @param _feeBeneficiary Must pass the address of account which receives fees acumulated.
   */
  constructor(
    address _feedAddress, 
    address _feeBeneficiary
  ) {
    priceFeed = AggregatorV3Interface(_feedAddress);
    feeBeneficiary = _feeBeneficiary;
  }

  struct FundEvent {
    uint256 id;
    string uri;
    string category;
    address event_address;
  }

  mapping(uint256 => bool) private _createdEvents;
  mapping(uint256 => FundEvent) private _fundEvents;

  event FeeChanged(uint8 new_fee);
  event FundEventCreated(uint256 _id, string _uri, string _category, uint256 _raiseAmount, address _address);

  /**
    @notice           Checks if event with the `_id` has already been created.
    @dev              This function checks if the event with same id has already been created.
    @param _id        Unique Id of the event
    @return checked   Must return boolean value
   */
  function _exists(uint256 _id) internal returns (bool checked) {
    return !_createdEvents[_id];
  }

  /**
    @notice           Checks if the parameter `_check` has string.
    @dev              This function checks if the given string parameter is empty or not.
    @param _check     contains string check value
    @return checked   Must return boolean value
   */
  function _hasString(string _check) internal returns (bool checked) {
    bytes memory stringTest = bytes(_check);
    return stringTest.length > 0;
  }

  /**
    @notice           Returns the latest price of native token in USD.
    @dev              Using price feed of Chainlink to get ETH value in USD 
    @return           Returns price value in integer
   */
  function getLatestPrice() public view returns (uint256) {
      (,uint256 price,,,) = priceFeed.latestRoundData();
      return price;
  }

  function getFeedDecimal() public view returns (uint8 decimal) {
    decimal = priceFeed.decimals();
  }

  /**
     * @notice        Create new crowdfunding event.
     * @dev           Creates a new crowdfunding event
     *
     * Emits a {FundEventCreated} event.
     * 
     * @param _uri            cannot be null or empty.
     * @param _category       cannot be null or empty.
     * @param _amountToRaise  cannot be less than or equals to 0.
     * @return                address of new crowdfunding event
     */
  function createEvent(
    string _uri, 
    string _category, 
    uint256 _amountToRaise, 
    address _fundReceivable
  ) public payable returns (address) {
    require(_hasString(_uri) && _hasString(_category), "InitiatorContract: Event parameter contains no value!");
    require(_amountToRaise > 0, "InitiatorContract: Please increase the event collection amount!");

    uint256 _id = counter++;
    uint8 decimal = getFeedDecimal(); 
    uint256 calc_amt = (fee * 10**decimal)/getLatestPrice(); // Calculate amount of tokens/coins required for fees.
    require(msg.value >= calc_amt, "InitiatorContract: Fee sent is not enough!");
    
    // Return back the excess amount of token sent
    if(msg.value > calc_amt) {
      uint256 excess_token = msg.value - calc_amt;
      (bool sent, ) = (msg.sender).call{value: excess_token}("");
      require(sent, "InitiatorContract: Failed to send back excess tokens!");
    }

    address newFundReceiver = (_fundReceivable == address(0)) ? msg.sender : _fundReceivable;

    address new_contract = _createEvent(_id, _uri, _category, _amountToRaise, newFundReceiver, feeBeneficiary);
    return new_contract;
  }

  /**
     * @dev Creates a new crowdfunding event.
     *
     * Emits a {FundEventCreated} event.
     */
  function _createEvent(
    uint256 _id, 
    string _uri, 
    string _category, 
    uint256 _amountToRaise, 
    address _fundReceivable,
    address _feeReceiver
  ) internal returns (address) {
    EventContract new_contract = new EventContract(_id, _uri, _amountToRaise, _fundReceivable, _feeReceiver);
    FundEvent memory new_event = FundEvent(_id, _uri, _category, new_contract);
    
    _fundEvents[_id] = new_event;
    _createdEvents[_id] = true;
    
    emit FundEventCreated(_id, _uri, _category, _amountToRaise, new_contract);
    return address(new_contract);
  }

  /**
     * @dev Change Fee of event creation.
     *
     * @param _fee New Fee value to set per funding event 
     *
     * Emits a {FeeChanged} event.
     */
  function changeFee(uint256 _fee) public onlyOwner {
    fee = _fee;
    emit FeeChanged(fee);
  }

  /**
     * @dev Change fee per transaction of the event.
     *
     * @param _event Address of event to check
     * @param _fee   New fee to set for the event
     */
  function changeChildTxnFees(address _event, uint256 _fee) public onlyOwner {
    EventContract(_event).changeTxnFee(_fee);
  }

  /**
     * @dev Change address of fee receiver.
     *
     * @param _address new address of fee receiver
     *
     */
  function changeFeeBeneficiary(address _address) public onlyOwner {
    feeBeneficiary = _address;
  }

  /**
     * @dev Send fees collected in the contract
     *
     * @param _to   Address to send fees to
     * @param amt   Amount of ETH to send
     */
  function distributeFees(address payable _to, uint256 amt) public onlyOwner {
    require(_to != address(0), "Cannot send to dead address!");
    (bool sent, ) = _to.call{ value: amt }("");
    require(sent, "Failed to send ETH!");
  }

  // Function to receive Ether. msg.data must be empty
  receive() external payable {}

  // Fallback function is called when msg.data is not empty
  fallback() external payable {}

}
